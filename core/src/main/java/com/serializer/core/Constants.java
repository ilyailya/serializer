package com.serializer.core;

class Constants {

    static final byte NULL_FIELD_VALUE = 0;
    static final byte CHAR_TYPE = 1;
    static final byte BYTE_TYPE = 2;
    static final byte SHORT_TYPE = 3;
    static final byte INT_TYPE = 4;
    static final byte LONG_TYPE = 5;
    static final byte FLOAT_TYPE = 6;
    static final byte DOUBLE_TYPE = 7;
    static final byte BOOLEAN_TYPE = 8;


    static final byte STRING_TYPE = 25;
    static final byte OBJECT_TYPE = 42;
    static final byte ARRAY_TYPE = 99;

    static final String NULL_OBJECT_TYPE = "NULL_OBJECT_TYPE";
    static final String ARRAY_OBJECT_TYPE = "ARRAY_OBJECT_TYPE";

}
