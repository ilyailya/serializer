package com.serializer.core;

import com.google.common.primitives.*;
import com.serializer.CompressionUtils;
import com.serializer.core.converter.Converter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

import static com.serializer.core.Constants.*;

//objectTypeSize-objectType-[fieldNameSize-fieldName-fieldType-fieldValueSize-fieldValue]
public class Serializer {

    private List<Byte> data = new ArrayList<>();
    private Object victim;
    private Set<Converter> converters = new HashSet<>();

    private Serializer(Object victim) {
        this.victim = victim;
    }

    Serializer(Object victim, Set<Converter> converters) {
        this.victim = victim;
        this.converters = converters;
    }

    public static Serializer getInstance(Object victim, Set<Converter> converters) {
        return new Serializer(victim, converters);
    }

    public static Serializer getInstance(Object victim) {
        return new Serializer(victim);
    }

    public byte[] serialize() throws IllegalAccessException {
        return Bytes.toArray(serializeToList());
    }

    public byte[] serializeCompressed() throws IllegalAccessException, IOException {
        byte[] bytes = serialize();
        return CompressionUtils.compress(bytes);
    }

    /**
     * <ol>
     * <li>preserving null values to serialize null values as collections elements</li>
     * <li>writing classname</li>
     * <li>processing primitive and wrappers / String / complex object</li>
     * </ol>
     *
     * @return list of bytes that may be added to another list
     * @throws IllegalAccessException if the field value is inaccessible
     */
    private List<Byte> serializeToList() throws IllegalAccessException {
        if (victim == null) {
            processString(NULL_OBJECT_TYPE);
            return data;
        }

        Class<?> clazz = victim.getClass();

        if (clazz.isArray()) {
            processString(ARRAY_OBJECT_TYPE);
            putArray((Object[]) victim, clazz);
            return data;
        }

        processString(clazz.getName());

        if (Character.class == clazz) {
            processByteArray(Chars.toByteArray((Character) victim));
        } else if (Byte.class == clazz) {
            data.add((Byte) victim);
        } else if (Short.class == clazz) {
            processByteArray(Shorts.toByteArray((Short) victim));
        } else if (Integer.class == clazz) {
            processByteArray(Ints.toByteArray((Integer) victim));
        } else if (Long.class == clazz) {
            processByteArray(Longs.toByteArray((Long) victim));
        } else if (Float.class == clazz) {
            putFloat((Float) victim);
        } else if (Double.class == clazz) {
            putDouble((Double) victim);
        } else if (Boolean.class == clazz) {
            putBoolean((Boolean) victim);
        } else if (String.class == clazz) {
            processString((String) victim);
        } else {
            Optional<Converter> suitableConverter = findSuitableConverter(clazz);
            if (suitableConverter.isPresent()) {
                processWithConverter(victim, suitableConverter.get());
            } else {
                processComplexObject(clazz);
            }
        }

        return data;
    }

    /**
     * For every non-static non-final field:
     * <ol>
     * <li>writing field name</li>
     * <li>writing field value</li>
     * </ol>
     *
     * @param clazz used to determine class fields
     * @throws IllegalAccessException if the field value is inaccessible
     */
    private void processComplexObject(Class<?> clazz) throws IllegalAccessException {
        Field[] fields = clazz.getDeclaredFields();

        for (Field f : fields) {
            int modifiers = f.getModifiers();
            if (Modifier.isFinal(modifiers) || Modifier.isStatic(modifiers)) continue;

            processString(f.getName());
            processFieldValue(f);
        }
    }

    private void processFieldValue(Field field) throws IllegalAccessException {
        field.setAccessible(true);
        Class<?> type = field.getType();

        if (type.isPrimitive()) {
            processPrimitive(field, type);
        } else {
            processComplexObject(field, type);
        }
    }

    @SuppressWarnings("unchecked")
    private void processWithConverter(Object value, Converter converter) throws IllegalAccessException {
        byte[] bytes = converter.write(value);
        putInt(bytes.length);
        processByteArray(bytes);
    }

    private Optional<Converter> findSuitableConverter(Class<?> type) {
        return converters.stream().filter(c -> c.supports(type)).findFirst();
    }

    private void putArray(Field field, Class<?> type) throws IllegalAccessException {
        Object[] array = (Object[]) field.get(victim);
        data.add(ARRAY_TYPE);
        putArray(array, type);
    }

    private void putArray(Object[] array, Class<?> type) throws IllegalAccessException {
        Class<?> componentType = type.getComponentType();
        processString(componentType.getName());
        putInt(array.length);

        for (Object element : array) {
            List<Byte> bytes = Serializer.getInstance(element, converters).serializeToList();
            putInt(bytes.size());
            data.addAll(bytes);
        }
    }

    /**
     * processing null value / String / array / complex objects
     */
    private void processComplexObject(Field field, Class<?> type) throws IllegalAccessException {
        Object rawValue = field.get(victim);
        if (rawValue == null) {
            data.add(NULL_FIELD_VALUE);
            return;
        }

        if (String.class == type) {
            data.add(STRING_TYPE);
            String fByte = (String) rawValue;
            processString(fByte);
        } else if (type.isArray()) {
            putArray(field, type);
        } else {
            data.add(OBJECT_TYPE);
            List<Byte> bytes = Serializer.getInstance(field.get(victim), converters).serializeToList();
            processSize(bytes);
            data.addAll(bytes);
        }
    }


    private void processPrimitive(Field f, Class<?> type) throws IllegalAccessException {
        if (Character.TYPE == type) {
            processChar(f.getChar(victim));
        } else if (Byte.TYPE == type) {
            processByte(f.getByte(victim));
        } else if (Short.TYPE == type) {
            processShort(f.getShort(victim));
        } else if (Integer.TYPE == type) {
            processInt(f.getInt(victim));
        } else if (Long.TYPE == type) {
            processLong(f.getLong(victim));
        } else if (Float.TYPE == type) {
            processFloat(f.getFloat(victim));
        } else if (Double.TYPE == type) {
            processDouble(f.getDouble(victim));
        } else if (Boolean.TYPE == type) {
            processBoolean(f.getBoolean(victim));
        }
    }

    private void processString(String value) {
        byte[] bytes = value.getBytes();
        processSize(bytes);
        processByteArray(bytes);
    }

    private void processByteArray(byte[] bytes) {
        for (byte b : bytes) {
            data.add(b);
        }
    }

    private void processSize(byte[] bytes) {
        int length = bytes.length;
        putInt(length);
    }

    private void processSize(List<Byte> bytes) {
        putInt(bytes.size());
    }

    private void processInt(int val) {
        data.add(INT_TYPE);
        putInt(val);
    }

    private void processChar(char value) {
        data.add(CHAR_TYPE);
        processByteArray(Chars.toByteArray(value));
    }

    private void processByte(byte value) {
        data.add(BYTE_TYPE);
        data.add(value);
    }

    private void processShort(short val) {
        data.add(SHORT_TYPE);
        processByteArray(Shorts.toByteArray(val));
    }

    private void putInt(int val) {
        processByteArray(Ints.toByteArray(val));
    }

    private void processLong(long val) {
        data.add(LONG_TYPE);
        processByteArray(Longs.toByteArray(val));
    }

    private void processFloat(float val) {
        data.add(FLOAT_TYPE);
        putFloat(val);
    }

    private void putFloat(float val) {
        putInt(Float.floatToRawIntBits(val));
    }

    private void processDouble(double val) {
        data.add(DOUBLE_TYPE);
        putDouble(val);
    }

    private void putDouble(double val) {
        long longBits = Double.doubleToRawLongBits(val);
        processByteArray(Longs.toByteArray(longBits));
    }

    private void processBoolean(boolean val) {
        data.add(BOOLEAN_TYPE);
        putBoolean(val);
    }

    private void putBoolean(boolean val) {
        byte result = (byte) (val ? 1 : 0);
        data.add(result);
    }

}
