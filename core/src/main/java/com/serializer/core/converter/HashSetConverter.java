package com.serializer.core.converter;

import com.serializer.core.DeSerializer;
import com.serializer.core.Serializer;

import java.util.Arrays;
import java.util.HashSet;

public class HashSetConverter implements Converter<HashSet> {

    @Override
    public HashSet read(byte[] data) throws Exception {
        Object[] arrayData = (Object[]) DeSerializer.getInstance(data).deserialize();

        return new HashSet<>(Arrays.asList(arrayData));
    }

    @Override
    public byte[] write(HashSet instance) throws IllegalAccessException {
        Object[] objects = instance.toArray();

        return Serializer.getInstance(objects).serialize();
    }

    @Override
    public boolean supports(Class clazz) {
        return HashSet.class == clazz;
    }
}
