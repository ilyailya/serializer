package com.serializer.core.converter;

public interface Converter<T> {

    T read(byte[] data) throws Exception;

    byte[] write(T instance) throws IllegalAccessException;

    boolean supports(Class clazz);
}
