package com.serializer.core;

import com.serializer.core.converter.Converter;

import java.util.HashSet;
import java.util.Set;

public class CoreFactory {

    private Set<Converter> converters = new HashSet<>();

    public Serializer getSerializerInstance(Object victim) {
        return new Serializer(victim, converters);
    }

    public <T> DeSerializer<T> getDeserializerInstance(byte[] data) {
        return new DeSerializer<>(data, converters);
    }

    public void registerConverter(Converter converter) {
        converters.add(converter);
    }
}
