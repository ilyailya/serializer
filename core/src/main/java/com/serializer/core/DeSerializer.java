package com.serializer.core;

import com.google.common.primitives.*;
import com.serializer.CompressionUtils;
import com.serializer.core.converter.Converter;
import org.apache.commons.lang3.mutable.MutableInt;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.serializer.core.Constants.*;
import static java.util.Arrays.copyOfRange;

public class DeSerializer<T> {

    private byte[] data;
    private Class<T> clazz;
    private MutableInt offset = new MutableInt();

    private Set<Converter> converters = new HashSet<>();

    private DeSerializer(byte[] data) {
        this.data = data;
    }

    DeSerializer(byte[] data, Set<Converter> converters) {
        this.data = data;
        this.converters = converters;
    }

    public static <T> DeSerializer<T> getInstance(byte[] data) {
        return new DeSerializer<>(data);
    }

    public static <T> DeSerializer<T> getInstance(byte[] data, Set<Converter> converters) {
        return new DeSerializer<>(data, converters);
    }

    @SuppressWarnings("unchecked")
    public T deserializeCompressed() throws Exception {
        data = CompressionUtils.decompress(data);
        return deserialize();
    }

    @SuppressWarnings("unchecked")
    public T deserialize() throws Exception {
        String className = retrieveString();

        if (NULL_OBJECT_TYPE.equals(className)) return null;
        if (ARRAY_OBJECT_TYPE.equals(className)) return (T) retrieveArray();

        clazz = (Class<T>) Class.forName(className);

        if (Character.class == clazz) {
            return (T) retrieveChar();
        } else if (Byte.class == clazz) {
            return (T) retrieveByte();
        } else if (Short.class == clazz) {
            return (T) retrieveShort();
        } else if (Integer.class == clazz) {
            return (T) retrieveInt();
        } else if (Long.class == clazz) {
            return (T) retrieveLong();
        } else if (Float.class == clazz) {
            return (T) retrieveFloat();
        } else if (Double.class == clazz) {
            return (T) retrieveDouble();
        } else if (Boolean.class == clazz) {
            return (T) retrieveBoolean();
        } else if (String.class == clazz) {
            return (T) retrieveString();
        } else {
            Optional<Converter> suitableConverter = findSuitableConverter(clazz);
            if (suitableConverter.isPresent()) {
                Converter<T> converter = suitableConverter.get();
                return retrieveWithConverter(converter);
            }
        }

        return processComplexObject();
    }

    private T retrieveWithConverter(Converter<T> converter) throws Exception {
        int length = retrieveInt();
        byte[] bytes = copyOfRange(this.data, offset.getValue(), offset.getValue() + length);
        return converter.read(bytes);
    }

    private Optional<Converter> findSuitableConverter(Class<?> type) {
        return converters.stream().filter(c -> c.supports(type)).findFirst();
    }

    private T processComplexObject() throws Exception {
        Constructor<T> constructor = clazz.getConstructor();
        T instance = constructor.newInstance();

        do {
            processField(instance);
        } while (offset.getValue() < data.length);

        return instance;
    }

    private void processField(T instance) throws Exception {
        String fieldName = retrieveString();
        Object fieldValue = retrieveFieldValue();
        Field declaredField = clazz.getDeclaredField(fieldName);
        declaredField.setAccessible(true);
        declaredField.set(instance, fieldValue);
    }

    private Object retrieveFieldValue() throws Exception {

        byte type = data[offset.getValue()];
        offset.increment();

        switch (type) {
            case CHAR_TYPE:
                return retrieveChar();
            case BYTE_TYPE:
                return retrieveByte();
            case SHORT_TYPE:
                return retrieveShort();
            case INT_TYPE:
                return retrieveInt();
            case LONG_TYPE:
                return retrieveLong();
            case FLOAT_TYPE:
                return retrieveFloat();
            case DOUBLE_TYPE:
                return retrieveDouble();
            case BOOLEAN_TYPE:
                return retrieveBoolean();
            case STRING_TYPE:
                return retrieveString();
            case ARRAY_TYPE:
                return retrieveArray();
            case OBJECT_TYPE:
                return retrieveObject();
            case NULL_FIELD_VALUE:
                return null;
            default:
                throw new IllegalArgumentException("Tried to deserialize the field of unsupported type: " + type);
        }
    }

    private Object retrieveArray() throws Exception {
        String fieldType = retrieveString();
        Class<?> componentType = Class.forName(fieldType);
        int arrayLength = retrieveInt();

        Object[] array = (Object[]) Array.newInstance(componentType, arrayLength);

        for (int i = 0; i < arrayLength; i++) {
            int elSize = retrieveInt();
            byte[] elBytes = copyOfRange(this.data, offset.getValue(), offset.getValue() + elSize);
            Object element = DeSerializer.getInstance(elBytes, converters).deserialize();
            offset.add(elSize);

            array[i] = element;
        }

        return array;
    }

    private Object retrieveObject() throws Exception {
        int length = retrieveInt();

        byte[] objectBytes = copyOfRange(this.data, offset.getValue(), offset.getValue() + length);
        Object result = DeSerializer.getInstance(objectBytes, converters).deserialize();
        offset.add(length);

        return result;
    }

    private String retrieveString() {
        int length = retrieveInt();

        byte[] rawString = copyOfRange(this.data, offset.getValue(), offset.getValue() + length);
        offset.add(length);

        return new String(rawString);
    }

    private Character retrieveChar() {
        char result = Chars.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 2));
        offset.add(2);

        return result;
    }

    private Byte retrieveByte() {
        byte result = data[offset.getValue()];
        offset.add(1);

        return result;
    }

    private Short retrieveShort() {
        short result = Shorts.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 2));
        offset.add(2);

        return result;
    }

    private Integer retrieveInt() {
        int result = Ints.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 4));
        offset.add(4);

        return result;
    }

    private Long retrieveLong() {
        long result = Longs.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 8));
        offset.add(8);

        return result;
    }

    private Float retrieveFloat() {
        int bits = Ints.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 8));
        offset.add(4);

        return Float.intBitsToFloat(bits);
    }

    private Double retrieveDouble() {
        long bits = Longs.fromByteArray(copyOfRange(data, offset.getValue(), offset.getValue() + 8));
        offset.add(8);

        return Double.longBitsToDouble(bits);
    }

    private Boolean retrieveBoolean() {
        byte rawValue = data[offset.getValue()];
        offset.increment();

        return rawValue == 1;
    }


}
