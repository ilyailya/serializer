package com.serializer;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;
import java.util.Set;

public class Dummy {

    private Character boxedChar;
    private Byte boxedByte;
    private Short boxedShort;
    private Integer boxedInt;
    private Long boxedLong;
    private Float boxedFloat;
    private Double boxedDouble;

    private Boolean boxedBoolean;

    private String first;
    private String last;
    private String _null;
    private String longest;

    private char primitiveChar;
    private byte primitiveByte;
    private short primitiveShort;
    private int primitiveInt;
    private long primitiveLong;

    private float primitiveFloat;
    private double primitiveDouble;

    private boolean primitiveBoolean;

    private String[] stringArray;
    private NestedDummy[] nestedDummyArray;
    private Integer[] boxedIntArray;

    private NestedDummy nestedDummy;

    private Dummy dummy;

    private Set<NestedDummy> nestedDummySet;
    private List<NestedDummy> nestedDummyList;

    public Dummy() {
    }


    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String get_null() {
        return _null;
    }

    public void set_null(String _null) {
        this._null = _null;
    }

    public String getLongest() {
        return longest;
    }

    public void setLongest(String longest) {
        this.longest = longest;
    }

    public char getPrimitiveChar() {
        return primitiveChar;
    }

    public void setPrimitiveChar(char primitiveChar) {
        this.primitiveChar = primitiveChar;
    }

    public byte getPrimitiveByte() {
        return primitiveByte;
    }

    public void setPrimitiveByte(byte primitiveByte) {
        this.primitiveByte = primitiveByte;
    }

    public short getPrimitiveShort() {
        return primitiveShort;
    }

    public void setPrimitiveShort(short primitiveShort) {
        this.primitiveShort = primitiveShort;
    }

    public int getPrimitiveInt() {
        return primitiveInt;
    }

    public void setPrimitiveInt(int primitiveInt) {
        this.primitiveInt = primitiveInt;
    }

    public long getPrimitiveLong() {
        return primitiveLong;
    }

    public void setPrimitiveLong(long primitiveLong) {
        this.primitiveLong = primitiveLong;
    }

    public float getPrimitiveFloat() {
        return primitiveFloat;
    }

    public void setPrimitiveFloat(float primitiveFloat) {
        this.primitiveFloat = primitiveFloat;
    }

    public double getPrimitiveDouble() {
        return primitiveDouble;
    }

    public void setPrimitiveDouble(double primitiveDouble) {
        this.primitiveDouble = primitiveDouble;
    }

    public boolean isPrimitiveBoolean() {
        return primitiveBoolean;
    }

    public void setPrimitiveBoolean(boolean primitiveBoolean) {
        this.primitiveBoolean = primitiveBoolean;
    }

    public Long getBoxedLong() {
        return boxedLong;
    }

    public void setBoxedLong(Long boxedLong) {
        this.boxedLong = boxedLong;
    }

    public Character getBoxedChar() {
        return boxedChar;
    }

    public void setBoxedChar(Character boxedChar) {
        this.boxedChar = boxedChar;
    }

    public Byte getBoxedByte() {
        return boxedByte;
    }

    public void setBoxedByte(Byte boxedByte) {
        this.boxedByte = boxedByte;
    }

    public Short getBoxedShort() {
        return boxedShort;
    }

    public void setBoxedShort(Short boxedShort) {
        this.boxedShort = boxedShort;
    }

    public Integer getBoxedInt() {
        return boxedInt;
    }

    public void setBoxedInt(Integer boxedInt) {
        this.boxedInt = boxedInt;
    }

    public Float getBoxedFloat() {
        return boxedFloat;
    }

    public void setBoxedFloat(Float boxedFloat) {
        this.boxedFloat = boxedFloat;
    }

    public Double getBoxedDouble() {
        return boxedDouble;
    }

    public void setBoxedDouble(Double boxedDouble) {
        this.boxedDouble = boxedDouble;
    }

    public Boolean getBoxedBoolean() {
        return boxedBoolean;
    }

    public void setBoxedBoolean(Boolean boxedBoolean) {
        this.boxedBoolean = boxedBoolean;
    }

    public String[] getStringArray() {
        return stringArray;
    }

    public void setStringArray(String[] stringArray) {
        this.stringArray = stringArray;
    }

    public NestedDummy[] getNestedDummyArray() {
        return nestedDummyArray;
    }

    public void setNestedDummyArray(NestedDummy[] nestedDummyArray) {
        this.nestedDummyArray = nestedDummyArray;
    }

    public Integer[] getBoxedIntArray() {
        return boxedIntArray;
    }

    public void setBoxedIntArray(Integer[] boxedIntArray) {
        this.boxedIntArray = boxedIntArray;
    }

    public NestedDummy getNestedDummy() {
        return nestedDummy;
    }

    public void setNestedDummy(NestedDummy nestedDummy) {
        this.nestedDummy = nestedDummy;
    }

    public Dummy getDummy() {
        return dummy;
    }

    public void setDummy(Dummy dummy) {
        this.dummy = dummy;
    }

    public Set<NestedDummy> getNestedDummySet() {
        return nestedDummySet;
    }

    public void setNestedDummySet(Set<NestedDummy> nestedDummySet) {
        this.nestedDummySet = nestedDummySet;
    }

    public List<NestedDummy> getNestedDummyList() {
        return nestedDummyList;
    }

    public void setNestedDummyList(List<NestedDummy> nestedDummyList) {
        this.nestedDummyList = nestedDummyList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Dummy dummy1 = (Dummy) o;

        return new EqualsBuilder()
                .append(primitiveChar, dummy1.primitiveChar)
                .append(primitiveByte, dummy1.primitiveByte)
                .append(primitiveShort, dummy1.primitiveShort)
                .append(primitiveInt, dummy1.primitiveInt)
                .append(primitiveLong, dummy1.primitiveLong)
                .append(primitiveFloat, dummy1.primitiveFloat)
                .append(primitiveDouble, dummy1.primitiveDouble)
                .append(primitiveBoolean, dummy1.primitiveBoolean)
                .append(boxedChar, dummy1.boxedChar)
                .append(boxedByte, dummy1.boxedByte)
                .append(boxedShort, dummy1.boxedShort)
                .append(boxedInt, dummy1.boxedInt)
                .append(boxedLong, dummy1.boxedLong)
                .append(boxedFloat, dummy1.boxedFloat)
                .append(boxedDouble, dummy1.boxedDouble)
                .append(boxedBoolean, dummy1.boxedBoolean)
                .append(first, dummy1.first)
                .append(last, dummy1.last)
                .append(_null, dummy1._null)
                .append(longest, dummy1.longest)
                .append(stringArray, dummy1.stringArray)
                .append(nestedDummyArray, dummy1.nestedDummyArray)
                .append(boxedIntArray, dummy1.boxedIntArray)
                .append(nestedDummy, dummy1.nestedDummy)
                .append(dummy, dummy1.dummy)
                .append(nestedDummySet, dummy1.nestedDummySet)
                .append(nestedDummyList, dummy1.nestedDummyList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(boxedChar)
                .append(boxedByte)
                .append(boxedShort)
                .append(boxedInt)
                .append(boxedLong)
                .append(boxedFloat)
                .append(boxedDouble)
                .append(boxedBoolean)
                .append(first)
                .append(last)
                .append(_null)
                .append(longest)
                .append(primitiveChar)
                .append(primitiveByte)
                .append(primitiveShort)
                .append(primitiveInt)
                .append(primitiveLong)
                .append(primitiveFloat)
                .append(primitiveDouble)
                .append(primitiveBoolean)
                .append(stringArray)
                .append(nestedDummyArray)
                .append(boxedIntArray)
                .append(nestedDummy)
                .append(dummy)
                .append(nestedDummySet)
                .append(nestedDummyList)
                .toHashCode();
    }
}
