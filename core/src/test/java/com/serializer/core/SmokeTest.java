package com.serializer.core;

import com.serializer.Dummy;
import com.serializer.NestedDummy;
import com.serializer.core.converter.HashSetConverter;
import org.junit.Before;
import org.junit.Test;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class SmokeTest {

    private CoreFactory coreFactory;

    @Before
    public void init() {
        HashSetConverter converter = new HashSetConverter();
        coreFactory = new CoreFactory();
        coreFactory.registerConverter(converter);
    }

    @Test
    public void smoke() throws Exception {
        String longString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        Dummy before = new Dummy();

        before.setFirst("abc");
        before.setLast("xyz");
        before.setLongest(longString);

        before.setPrimitiveBoolean(false);
        before.setBoxedLong(25L);

        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Dummy> deser = DeSerializer.getInstance(bytes);
        Dummy after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smoke2() throws Exception {
        Dummy before = new Dummy();
        before.setPrimitiveChar('r');
        before.setPrimitiveByte((byte) 35);
        before.setPrimitiveShort((short) 14);
        before.setPrimitiveInt(23);
        before.setPrimitiveLong(124L);
        before.setFirst("lol");
        before.setPrimitiveBoolean(true);

        byte[] bytes = coreFactory.getSerializerInstance(before).serialize();

        DeSerializer<Dummy> deser = coreFactory.getDeserializerInstance(bytes);
        Dummy after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smoke3() throws Exception {
        String before = "valueeee";
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<String> deser = DeSerializer.getInstance(bytes);
        String after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smoke4() throws Exception {
        int before = 25;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Integer> deser = DeSerializer.getInstance(bytes);
        Integer after = deser.deserialize();

        assertEquals(before, (int) after);
    }

    @Test
    public void smoke5() throws Exception {
        Dummy before = new Dummy();

        NestedDummy dummy = new NestedDummy();
        dummy.setHeight(2222L);
        dummy.setName("gdsfgdfg");
        dummy.setList(new ArrayList<>(Arrays.asList("s", "asd")));

        before.setNestedDummy(dummy);

        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Dummy> deser = DeSerializer.getInstance(bytes);
        Dummy after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smoke6() throws Exception {
        List<String> before = new ArrayList<>();
        before.add("asdas");
        before.add(null);
        before.add("bvnvgnd");
        before.add(null);
        before.add("ee56yu");

        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<List<String>> deser = DeSerializer.getInstance(bytes);
        List<String> after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smoke7() throws Exception {
        Set<String> before = new HashSet<>();
        before.add("asdas");
        before.add(null);
        before.add("bvnvgnd");
        before.add(null);
        before.add("ee56yu");

        byte[] bytes = coreFactory.getSerializerInstance(before).serialize();

        DeSerializer<Set<String>> deser = coreFactory.getDeserializerInstance(bytes);
        Set<String> after = deser.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smokeComplexObject() throws Exception {
        PodamFactory factory = new PodamFactoryImpl();
        Dummy before = factory.manufacturePojo(Dummy.class);

        byte[] bytes = coreFactory.getSerializerInstance(before).serialize();
        DeSerializer<Dummy> deSerializer = coreFactory.getDeserializerInstance(bytes);
        Dummy after = deSerializer.deserialize();

        assertEquals(before, after);
    }

    @Test
    public void smokeComplexObjectCompressed() throws Exception {
        PodamFactory factory = new PodamFactoryImpl();
        Dummy before = factory.manufacturePojo(Dummy.class);

        byte[] bytes = coreFactory.getSerializerInstance(before).serializeCompressed();
        DeSerializer<Dummy> deSerializer = coreFactory.getDeserializerInstance(bytes);
        Dummy after = deSerializer.deserializeCompressed();

        assertEquals(before, after);
    }

}
