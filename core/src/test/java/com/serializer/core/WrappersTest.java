package com.serializer.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(value = Parameterized.class)
public class WrappersTest {

    private Object victim;

    public WrappersTest(Object victim) {
        this.victim = victim;
    }

    @Parameters()
    public static Collection<Object> data() {
        return Arrays.asList(
                'f', (byte) 2, (short) 15, 25, 325L, 33.3F, 4365.435D, Float.NaN, Double.NaN, true, false
        );
    }

    @Test
    public void test() throws Exception {
        byte[] bytes = Serializer.getInstance(victim).serialize();
        Object result = DeSerializer.getInstance(bytes).deserialize();

        assertEquals(victim, result);
    }

}
