package com.serializer.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(value = Parameterized.class)
public class ArrayTest {

    private Object[] victim;

    public ArrayTest(Object[] victim) {
        this.victim = victim;
    }

    @Parameters()
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {new Byte[]{1, 5}},
                {new String[]{"q", "das0", null, "", null, "21"}},
                {null},
                {new Integer[]{345, 213, 0, 5}}}
        );

    }

    @Test
    public void test() throws Exception {
        byte[] bytes = Serializer.getInstance(victim).serialize();
        Object[] result = (Object[]) DeSerializer.getInstance(bytes).deserialize();

        assertArrayEquals(victim, result);
    }

}
