package com.serializer.core;

import org.junit.Assert;
import org.junit.Test;

public class PrimitiveTest {

    @Test
    public void shouldProcessChar() throws Exception {
        char before = 'e';
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Character> deser = DeSerializer.getInstance(bytes);
        Character after = deser.deserialize();

        Assert.assertEquals(before, (char) after);
    }

    @Test
    public void shouldProcessByte() throws Exception {
        byte before = 34;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Byte> deser = DeSerializer.getInstance(bytes);
        Byte after = deser.deserialize();

        Assert.assertEquals(before, (byte) after);
    }

    @Test
    public void shouldProcessShort() throws Exception {
        short before = 354;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Short> deser = DeSerializer.getInstance(bytes);
        Short after = deser.deserialize();

        Assert.assertEquals(before, (short) after);
    }

    @Test
    public void shouldProcessInt() throws Exception {
        byte[] bytes = Serializer.getInstance(25).serialize();

        DeSerializer<Integer> deser = DeSerializer.getInstance(bytes);
        Integer after = deser.deserialize();

        Assert.assertEquals(25, (int) after);
    }

    @Test
    public void shouldProcessLongChar() throws Exception {
        long before = 345353645L;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Long> deser = DeSerializer.getInstance(bytes);
        Long after = deser.deserialize();

        Assert.assertEquals(before, (long) after);
    }

    @Test
    public void shouldProcessFloat() throws Exception {
        float before = 3242352.232342F;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Float> deser = DeSerializer.getInstance(bytes);
        Float after = deser.deserialize();

        Assert.assertEquals(before, after, 0);
    }

    @Test
    public void shouldProcessDouble() throws Exception {
        double before = 3242352.232342D;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Double> deser = DeSerializer.getInstance(bytes);
        Double after = deser.deserialize();

        Assert.assertEquals(before, after, 0);
    }

    @Test
    public void shouldProcessBoolean() throws Exception {
        boolean before = true;
        byte[] bytes = Serializer.getInstance(before).serialize();

        DeSerializer<Boolean> deser = DeSerializer.getInstance(bytes);
        Boolean after = deser.deserialize();

        Assert.assertEquals(before, after);
    }

}
