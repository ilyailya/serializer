### Serializer project

This project is my attempt to create a custom serializer and to compare it to existing serializers such as Kryo or JSON.

The approach is pretty straightforward:

- result contains meta information such as class name, field name or field type

- field type is stored as a single byte

- fields are stored using the following scheme:

    1. fieldName data size
    2. fieldName data
    3. fieldType (core-specific constant)
    4. fieldValue data size
    5. fieldValue data
    
- arrays are stored using the following scheme:

    1. fieldName data size
    2. fieldName data
    3. fieldType (ARRAY_TYPE)
    4. array Java type data size
    5. array Java type data
    6. array length
    7. for every element - serialize() is called as for single object
    
- complex fields are serialized as single objects using their own serializers

- there's an ability to implement your own Converter to be used by serializer to convert data to byte[] and back.


### Performance

![Simple type processing benchmark](charts/Simple_type_processing.png)
![Collections processing benchmark](charts/collections.png)

- For the complex object processing the chart includes both serialize/deserialize performance charts as well as the data size.
Chart also includes the usage of serializer with built-in data compression feature.

![Collections processing benchmark](charts/complex_object.png)
