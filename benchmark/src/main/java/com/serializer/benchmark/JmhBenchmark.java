package com.serializer.benchmark;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.serializer.Dummy;
import com.serializer.core.CoreFactory;
import com.serializer.core.converter.HashSetConverter;
import org.codehaus.jackson.map.ObjectMapper;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.serializer.benchmark.DataGeneratorUtil.*;
import static java.util.concurrent.TimeUnit.MICROSECONDS;

@State(Scope.Thread)
public class JmhBenchmark {

    private List<String> stringList;
    private List<String> stringListBig;
    private Set<String> stringSet;
    private int primitiveInt;
    private Float boxedFloat;
    private String string;
    private Integer[] boxedIntegerArray;
    private Dummy dummy;

    private ObjectMapper jsonMapper = new ObjectMapper();
    private Kryo kryo = new Kryo();
    private CoreFactory coreFactory;

    private byte[] kryoBytes;
    private byte[] jsonBytes;
    private byte[] coreBytes;
    private byte[] coreCompressedBytes;

    @Setup(Level.Trial)
    public void init() throws IOException, IllegalAccessException {
        Random random = new Random();

        stringList = generateStringList(25);
        stringListBig = generateStringList(1000);
        stringSet = generateStringSet(25);

        primitiveInt = random.nextInt();
        boxedFloat = random.nextFloat();

        string = generateString(random);

        boxedIntegerArray = generateBoxedIntArray(25);

        dummy = generateDummy();

        HashSetConverter converter = new HashSetConverter();
        coreFactory = new CoreFactory();
        coreFactory.registerConverter(converter);

        Dummy deserializeDummy = generateDummy();
        kryoBytes = serializeWithKryo(deserializeDummy);
        jsonBytes = jsonMapper.writeValueAsBytes(deserializeDummy);
        coreBytes = coreFactory
                .getSerializerInstance(deserializeDummy)
                .serialize();
        coreCompressedBytes = coreFactory
                .getSerializerInstance(deserializeDummy)
                .serializeCompressed();
    }

    @Benchmark
    public void jsonStringList() throws IOException {
        jsonMapper.writeValueAsBytes(stringList);
    }

    @Benchmark
    public void kryoStringList() {
        serializeWithKryo(stringList);
    }

    @Benchmark
    public void coreStringList() throws IllegalAccessException {
        coreFactory.getSerializerInstance(stringList).serialize();
    }

    @Benchmark
    public void jsonStringListBig() throws IOException {
        jsonMapper.writeValueAsBytes(stringListBig);
    }

    @Benchmark
    public void kryoStringListBig() {
        serializeWithKryo(stringListBig);
    }

    @Benchmark
    public void coreStringListBig() throws IllegalAccessException {
        coreFactory.getSerializerInstance(stringListBig).serialize();
    }

    @Benchmark
    public void jsonStringSet() throws IOException {
        jsonMapper.writeValueAsBytes(stringSet);
    }

    @Benchmark
    public void kryoStringSet() {
        serializeWithKryo(stringSet);
    }

    @Benchmark
    public void coreStringSet() throws IllegalAccessException {
        coreFactory.getSerializerInstance(stringSet).serialize();
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void jsonString() throws IOException {
        jsonMapper.writeValueAsBytes(string);
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void kryoString() {
        serializeWithKryo(string);
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void coreString() throws IllegalAccessException {
        coreFactory.getSerializerInstance(string).serialize();
    }

    @Benchmark
    public void jsonBoxedIntArray() throws IOException {
        jsonMapper.writeValueAsBytes(boxedIntegerArray);
    }

    @Benchmark
    public void kryoBoxedIntArray() {
        serializeWithKryo(boxedIntegerArray);
    }

    @Benchmark
    public void coreBoxedIntArray() throws IllegalAccessException {
        coreFactory.getSerializerInstance(boxedIntegerArray).serialize();
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void jsonPrimitiveInt() throws IOException {
        jsonMapper.writeValueAsBytes(primitiveInt);
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void kryoPrimitiveInt() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Output output = new Output(stream);
        kryo.writeObject(output, primitiveInt);
        output.close();

        stream.toByteArray();
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void corePrimitiveInt() throws IllegalAccessException {
        coreFactory.getSerializerInstance(primitiveInt).serialize();
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void jsonBoxedFloat() throws IOException {
        jsonMapper.writeValueAsBytes(boxedFloat);
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void kryoBoxedFloat() {
        serializeWithKryo(boxedFloat);
    }

    @Benchmark
    @OutputTimeUnit(MICROSECONDS)
    public void coreBoxedFloat() throws IllegalAccessException {
        coreFactory.getSerializerInstance(boxedFloat).serialize();
    }

    @Benchmark
    public void jsonComlexObject() throws IOException {
        jsonMapper.writeValueAsBytes(dummy);
    }

    @Benchmark
    public void kryoComplexObject() {
        serializeWithKryo(dummy);
    }

    @Benchmark
    public void coreComplexObject() throws IllegalAccessException {
        coreFactory.getSerializerInstance(dummy).serialize();
    }

    @Benchmark
    public void coreCompressedComplexObject() throws IllegalAccessException, IOException {
        coreFactory.getSerializerInstance(dummy).serializeCompressed();
    }

    @Benchmark
    public void jsonComlexObjectDes() throws IOException {
        jsonMapper.readValue(jsonBytes, Dummy.class);
    }

    @Benchmark
    public void kryoComplexObjectDes() {
        ByteArrayInputStream ins = new ByteArrayInputStream(kryoBytes);
        Input input = new Input(ins);

        Dummy after = kryo.readObject(input, Dummy.class);
    }

    @Benchmark
    public void coreComplexObjectDes() throws Exception {
        coreFactory.getDeserializerInstance(coreBytes).deserialize();
    }

    @Benchmark
    public void coreCompressedComplexObjectDes() throws Exception {
        coreFactory.getDeserializerInstance(coreCompressedBytes).deserializeCompressed();
    }


    private byte[] serializeWithKryo(Object object) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Output output = new Output(stream);
        kryo.writeObject(output, object);
        output.close();

        return stream.toByteArray();
    }

    public static void main(String[] args) throws RunnerException {

        Options options = new OptionsBuilder()
                .include(JmhBenchmark.class.getSimpleName()).threads(1)
                .mode(Mode.Throughput).warmupIterations(3).measurementIterations(5)
                .warmupTime(TimeValue.seconds(1))
                .measurementTime(TimeValue.seconds(1))
                .timeUnit(TimeUnit.MILLISECONDS)
                .forks(1).shouldFailOnError(true).shouldDoGC(true)
                .jvmArgs("-server").build();
        new Runner(options).run();

    }
}