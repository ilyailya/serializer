package com.serializer.benchmark;

import com.serializer.Dummy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.nio.charset.Charset;
import java.util.*;

public class DataGeneratorUtil {

    public static List<String> generateStringList(int size) {
        List<String> result = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            result.add(generateString(random));
        }

        return result;
    }

    public static Set<String> generateStringSet(int size) {
        Set<String> result = new HashSet<>();
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            result.add(generateString(random));
        }

        return result;
    }

    public static Dummy generateDummy() {
        PodamFactory factory = new PodamFactoryImpl();

        return factory.manufacturePojo(Dummy.class);
    }

    public static Integer[] generateBoxedIntArray(int size) {
        Integer[] result = new Integer[size];
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            result[i] = random.nextInt();
        }

        return result;
    }

    public static String generateString(Random random) {
        byte[] array = new byte[7];
        random.nextBytes(array);

        return new String(array, Charset.forName("UTF-8"));
    }

}
