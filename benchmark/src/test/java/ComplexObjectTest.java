import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.serializer.Dummy;
import com.serializer.benchmark.DataGeneratorUtil;
import com.serializer.core.CoreFactory;
import com.serializer.core.DeSerializer;
import com.serializer.core.converter.HashSetConverter;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ComplexObjectTest {

    private static Dummy dummy;
    private static Logger logger = LoggerFactory.getLogger(ComplexObjectTest.class);

    private CoreFactory coreFactory;

    @Before
    public void init() {
        HashSetConverter converter = new HashSetConverter();
        coreFactory = new CoreFactory();
        coreFactory.registerConverter(converter);
    }

    @BeforeClass
    public static void initClass() {
        dummy = DataGeneratorUtil.generateDummy();
    }

    @Test
    public void coreTest() throws Exception {
        byte[] bytes = coreFactory.getSerializerInstance(dummy).serialize();
        logger.info("Core: {}", bytes.length);

        DeSerializer<Dummy> deserializerInstance = coreFactory.getDeserializerInstance(bytes);
        Dummy after = deserializerInstance.deserialize();

        assertEquals(dummy, after);
    }

    @Test
    public void coreCompressedTest() throws Exception {
        byte[] bytes = coreFactory.getSerializerInstance(dummy).serializeCompressed();
        logger.info("Core compressed: {}", bytes.length);

        DeSerializer<Dummy> deserializerInstance = coreFactory.getDeserializerInstance(bytes);
        Dummy after = deserializerInstance.deserializeCompressed();

        assertEquals(dummy, after);
    }

    @Test
    public void jsonTest() throws IOException {
        ObjectMapper jsonMapper = new ObjectMapper();
        byte[] bytes = jsonMapper.writeValueAsBytes(dummy);

        logger.info("JSON: {}", bytes.length);

        Dummy after = jsonMapper.readValue(bytes, Dummy.class);

        assertEquals(dummy, after);
    }

    @Test
    public void kryoTest() {

        Kryo kryo = new Kryo();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Output output = new Output(stream);
        kryo.writeObject(output, dummy);
        output.close();
        byte[] bytes = stream.toByteArray();

        logger.info("Kryo: {}", bytes.length);

        ByteArrayInputStream ins = new ByteArrayInputStream(bytes);
        Input input = new Input(ins);

        Dummy after = kryo.readObject(input, Dummy.class);

        assertEquals(dummy, after);
    }


}
